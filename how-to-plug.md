º1 Retire la funda exterior del cable de alimentación

º2 acorte el cable marron 

º3 Retire el aislamiento de los núcleos internos

º4 Retuerza las cables y dóblelas

º5 Afloje los tornillos en el alivio de tensión 

º6 Alimente el cable marrón en la terminal L

º7 Conecte el cable de tierra verde/amarillo en el terminal tierra

º8 finalmente conecte el cable azul a la terminal neutro

º9 Apriete el alivio de tensión y vuelva a colocar la cubierta

Hay 3 imágenes que te mostrarán qué es el terminal  (L LIVE) (N neutro) (E Earth/tierra) 
y otras cosas mas
![ALT](how to plug.jpg)
![ALT](How-to-Wire-a-Plug.jpg)
![ALT](how to plug image.jpeg)
